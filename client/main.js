import {Template} from 'meteor/templating';
import './main.html';

import { CheckRoomPresence } from 'meteor/media-sharing/lib';
import { ScreenSharingHandler } from 'meteor/media-sharing';

Template.container.onCreated(function(){
    this.payload = { roomid:"theidroom", instanceId:"theInstanceId" };
    this.checker = CheckRoomPresence({roomid:this.payload.roomid , domain:""});
});

Template.container.helpers({
    state(){
        return Template.instance().checker.state.get();
    },
    payload(){
        return Template.instance().payload;
    }
})
Template.container.events({
    'click #star'(evt,ins){
        
    },
    'click #open'(evt,ins){

    }
})