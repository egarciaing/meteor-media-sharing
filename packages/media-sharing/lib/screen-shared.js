/*global io*/

import RecordRTC from 'recordrtc';
import Broadcast from './broadcast';
import 'webrtc-screen-capturing/getScreenId';

export class ScalableScreenSharing extends Broadcast {
	constructor(){
		super(...arguments);
		this.__muted = false;
        this.muted = new ReactiveVar(this.__muted);

		this.enableRecordings = false;
   		this.allRecordedBlobs = [];
    	this.Preview = null;
        
        this.connection.session = {
            screen: true,
            oneway: true
        };

    	this.connection.getScreenConstraints = (callback)=> {
		    getScreenConstraints((error, screen_constraints)=> {
		        if (!error) callback(error, this.connection.modifyScreenConstraints(screen_constraints));
		    });
		};

	    this.Connected = this.connection.connectSocket;
	    this.Streaming = (onstream)=> this.connection.onstream=onstream;
	    this.onLeave = (onleave)=> this.connection.onleave=onleave;
	}


	// experimental
	removeStreams(){
		this.connection.attachStreams.forEach(stream => {
            stream.getTracks().forEach(track => track.stop())
        });
	}

	getBroadcastMetadata(force_extra = {}){
		return {
            broadcastId:this.roomid,
            roomid:this.roomid,
            userid:this.connection.userid,
            typeOfStreams: this.connection.session,
            ...force_extra
        };
	}

	OpenOrJoin(){
		this.connection.getSocket((socket) => {
            socket.emit('check-broadcast-presence', this.roomid, (isBroadcastExists) =>{
                if(!isBroadcastExists) {
                	this.setUserId(this.roomid, ()=>{
               			socket.emit('join-broadcast', this.getBroadcastMetadata());            	
                    });                	
                }else{
               		socket.emit('join-broadcast', this.getBroadcastMetadata());
                }
            });
        });
	}

	RecordStream(stream){
		if(!this.enableRecordings) {
            return;
        }

        this.connection.currentRecorder = RecordRTC(stream, {
            type: 'video'
        });

        this.connection.currentRecorder.startRecording();

        setTimeout(()=>{
            if(this.connection.isUpperUserLeft || !this.connection.currentRecorder) {
                return;
            }

            this.connection.currentRecorder.stopRecording(()=>{
                this.allRecordedBlobs.push(this.connection.currentRecorder.blob);

                if(this.connection.isUpperUserLeft) {
                    return;
                }

                this.connection.currentRecorder = null;
                this.RecordStream(stream);
            });
        }, 30 * 1000); // 30-seconds
	}

	mute(target){
		/*this.connection.attachStreams.forEach(stream => {
			if(stream.isAudio){
				stream[this.__muted ? 'unmute' : 'mute']('audio');
				this.__muted = !this.__muted;
                this.muted.set(this.__muted);
			}
        });*/

        const event = this.connection.streamEvents.selectFirst({ isAudio: true });
        event.stream[this.__muted ? 'unmute' : 'mute']('audio');
        this.__muted = !this.__muted;
        this.muted.set(this.__muted);
	}

	disableVideo(){
		this.connection.attachStreams.forEach(stream => {
            if(stream.isVideo||stream.isScreen){
            	stream.getTracks().forEach(track => {
	            	if( track.kind == 'video' ) track.enabled = !track.enabled;
	            });
            }
        });
	}

    snapshot(){
        const event = this.connection.streamEvents.selectFirst({ isVideo: true });
        const imageCapture = new ImageCapture(event.stream.getVideoTracks()[0]);
       
        return imageCapture.grabFrame().then(function(imageBitMap) {
            let canvas = document.createElement('canvas');
            canvas.width = imageBitMap.width;
            canvas.height = imageBitMap.height;
            
            let ctx = canvas.getContext("2d");
            ctx.drawImage(imageBitMap, 0, 0, imageBitMap.width,imageBitMap.height);

            return new Promise((resolve, reject) =>{
                canvas.toBlob(resolve);
            }).then((blob) => {
                let base64 = canvas.toDataURL('image/jpeg');
                return { blob, base64 }
            });

        }).catch(function(error){
            console.error('takeSnapshot: ',error);
        });
    }
}