export const MediaSource = new ReactiveVar('screen');
export const MediaStatus = new ReactiveVar('unknow');
export var ExtensionId = "ajhifddimkapgcifgcodmmfdlknahffk";

export function getExtensionStatus(extensionid, callback) {
    if (DetectRTC.browser.name === 'Firefox') {
        MediaStatus.set('not-chrome');
        return callback('not-chrome');
    }

    if (arguments.length != 2) {
        callback = extensionid;
        extensionid = ExtensionId;
    }

    var image = document.createElement('img');
    image.src = 'chrome-extension://' + extensionid + '/icon.png';
    
    image.onload = function() {
        MediaSource.set('screen');

        window.postMessage('are-you-there', '*');
        Meteor.setTimeout(function() {
            if (MediaSource.get() == 'screen') {
                
                MediaStatus.set('installed-disabled');
                callback('installed-disabled');
            } else{

                MediaStatus.set('installed-enabled');
                callback('installed-enabled');
            }
        }, 2000);
    };

    image.onerror = function() {
        MediaStatus.set('not-installed');
        callback('not-installed');
    };
}

export function isExtensionAvailable(callback) {
    if (!callback) return;

    if (MediaSource.get() == 'desktop') return callback(true);

    // ask extension if it is available
    window.postMessage('are-you-there', '*');

    Meteor.setTimeout(function() {
        if (MediaSource.get() == 'screen') {
            callback(false);
        } else {
            callback(true);
        }
    }, 2000);
}

function onMessageCallback(data) {
    // "cancel" button is clicked
    if (data == 'PermissionDeniedError') {
        MediaSource.set('PermissionDeniedError');
	    throw new Error('PermissionDeniedError');
    }

    // extension notified his presence
    if (data == 'rtcmulticonnection-extension-loaded') {
        MediaSource.set('desktop');
    }
}

window.addEventListener('message', function(event) {
    if (event.origin != window.location.origin) return;
    onMessageCallback(event.data);

    if (DetectRTC.browser.name === 'Firefox') {
       return MediaStatus.set('not-chrome');
    }

    var image = document.createElement('img');
    image.src = 'chrome-extension://' + ExtensionId + '/icon.png';
    
    image.onload = function() {
        Meteor.setTimeout(function() {
            if (MediaSource.get() == 'screen') {
                MediaStatus.set('installed-disabled');
            } else{
                MediaStatus.set('installed-enabled');
            }
        }, 2000);
    };

    image.onerror = function() {
        MediaStatus.set('not-installed');
    };
});