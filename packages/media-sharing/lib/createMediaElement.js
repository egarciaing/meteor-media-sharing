export function CreateMediaElement(event,config){
    const mediaElement = config&&config.element || document.createElement('video');
    var streamRef = null;

    try {
        mediaElement.setAttributeNode(document.createAttribute('autoplay'));
        mediaElement.setAttributeNode(document.createAttribute('playsinline'));
    } catch (e) {
        mediaElement.setAttribute('autoplay', true);
        mediaElement.setAttribute('playsinline', true);
    }


    function setSourceStream(element,stream){
        if ('srcObject' in element) {
            element.srcObject = stream;
        } else {
           if(stream != null) element[!!navigator.mozGetUserMedia ? 'mozSrcObject' : 'src'] = !!navigator.mozGetUserMedia ? stream : (window.URL || window.webkitURL).createObjectURL(stream);
        }
    }

    mediaElement.removeSourceStream = () => {
        mediaElement.userid = '';
        mediaElement.id = '';
        mediaElement.eventType = '';
        setSourceStream(mediaElement,null);
    }

    mediaElement.setSourceStream = e => {
        // prevent origin overload stream 
        if(mediaElement.eventType == e.type) return;
        mediaElement.local(e.type=='local');

        mediaElement.userid = e.userid;
        mediaElement.id = e.streamid;
        mediaElement.eventType = e.type;
        setSourceStream(mediaElement,e.stream);
    }

    /*if the source stream is local type*/
    mediaElement.isLocal = true;
    mediaElement.local = isLocal =>{

        mediaElement.isLocal = isLocal;
        if(isLocal) {
            mediaElement.volume = 0;
            mediaElement.muted = true;
            try{
                mediaElement.setAttributeNode(document.createAttribute('muted'));
            }catch(e){
                mediaElement.setAttribute('muted', true);
            }
        }else{
            mediaElement.volume = 1;
            mediaElement.muted = false;
            mediaElement.removeAttribute('muted');
        }           
    }

    mediaElement.dettach = ()=>{
        streamRef = 'srcObject' in mediaElement ? mediaElement.srcObject : mediaElement.src;
        mediaElement.parentNode.removeChild(mediaElement);
    }
    mediaElement.attach=(container)=>{
        mediaElement['srcObject' in mediaElement && !!streamRef ? 'srcObject' : 'src' ] = streamRef;
        container.appendChild(mediaElement);
    }
    mediaElement.attached=()=>{
        return !!mediaElement.parentNode;
    }
    
    mediaElement.hasStreamId = streamid => (streamid+'')==mediaElement.id;
    mediaElement.hasSource = () => !!mediaElement.srcObject;

    mediaElement.local(config&&config.local);
    if(!!config.parent) mediaElement.attach(config.parent);
    if(!!event) mediaElement.setSourceStream(event);

    return mediaElement;
}
