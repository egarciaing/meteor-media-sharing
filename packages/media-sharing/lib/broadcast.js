import { Common } from './common';
import { CheckPresence } from './checkPresence';
import ScreenSharingHandler from '../client/main';

import RTCMultiConnection from '../imports/RTCMultiConnection';

export default class Broadcast extends Common {
	constructor(roomid,options){

		super();

		this.roomid = roomid;
		this.options=options;
		this.owner = new ReactiveVar({});

		this.connection = new RTCMultiConnection(this.roomid,options);

		this.connection.enableScalableBroadcast = true;
	    this.connection.autoCloseEntireSession = true;
	    this.connection.maxRelayLimitPerUser = 1;
	    
	    if(ScreenSharingHandler.getIceServers()) {
	    	this.connection.iceServers = ScreenSharingHandler.getIceServers();
	    }

	    this.connection.socketMessageEvent = 'scalable-broadcast';
		this.connection.direction = 'one-to-many';
	    this.connection.socketURL =  '/';

		this.connection.extra.broadcastId = roomid;
		this.connection.onstreamended = (stream) =>{
	        this.notifyRoomState('close');
	    };
	    
	    this.connection.onUserStatusChanged = (event)=>{
	    	console.log('onUserStatusChanged',event);
	    	if(event.userId == this.roomid){
	    		//this.owner.set({ ...event.extra, status:event.status });
	    	}
	    }

	    this.disconnect = this.connection.disconnect;
	    this.closeSocket = this.connection.closeSocket;
		this.getSocket = this.connection.getSocket;

	    this.isRoomExist = callback => this.connection.checkPresence(this.roomid,callback);
	}

	notifyRoomState(status){
		const alive = {'open':true,'close':false}[status];
		if(status!=undefined){
			this.connection.getSocket( socket =>{
				 socket.emit('room-status-changed',this.roomid, {
					owner:this.connection.extra,
					alive,
					status,
					roomType:'Broadcast'
				});
			})
		}
	}

    setRoomId(id){
    	this.roomid = id;
    	this.connection.extra.broadcastId = roomid;
    }
}