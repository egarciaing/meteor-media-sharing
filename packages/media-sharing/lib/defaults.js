/*
 * call methods
 * BroadcastConnectedEvents.bind(ScalableScreenSharing instance)(socket) or
 * BroadcastConnectedEvents.call(ScalableScreenSharing instance, socket)
 */
export function BroadcastConnectedEvents(socket){

    /* set user data */
    const user =  Meteor && Meteor.user && Meteor.user();
    if(!!user){
        this.connection.extra = { 
            _id:user._id,
            name:user.name, 
            username:user.username,
            createAt: (new Date).toISOString()
        };

        this.connection.updateExtraData();   
    }

    socket.on('join-broadcaster', (hintsToJoinBroadcast)=> {
        console.log('join-broadcaster', hintsToJoinBroadcast);

        this.connection.session = hintsToJoinBroadcast.typeOfStreams;
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveVideo: true,
            OfferToReceiveAudio: true
        };

        this.connection.join(hintsToJoinBroadcast.userid);
    });

    socket.on('rejoin-broadcast', (broadcastId)=> {
        console.log('rejoin-broadcast', broadcastId);

        this.connection.attachStreams = [];
        socket.emit('check-broadcast-presence', broadcastId, (isBroadcastExists) =>{
            if(!isBroadcastExists) {
               this.connection.userid = broadcastId;
            }
            socket.emit('join-broadcast', this.getBroadcastMetadata({broadcastId}));
        });
    });

    socket.on('start-broadcasting', (typeOfStreams) => {
        console.log('start-broadcasting', typeOfStreams);

        // host i.e. sender should always use this!
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveVideo: false,
            OfferToReceiveAudio: false
        };

        this.connection.session = typeOfStreams;

        this.connection.captureUserMedia((audioStream) =>{
            this.connection.captureUserMedia((screenStream) =>{
                screenStream.addTrack(audioStream.getAudioTracks()[0]);
                this.connection.dontCaptureUserMedia = true;
                this.connection.openOrJoin(this.connection.userid, () =>{
                    this.notifyRoomState('open');// notify room status
                });
            }, {screen: true});
        }, {audio: true});
    });

    socket.on('broadcast-stopped', (roomid)=>{
        this.stop();
    });
}

