let checker = null;

export function CheckRoomPresence(config){
	if(!checker){
		checker = CheckPresence(config);
	}else{
		config&&config.roomid&&checker.setRoomId(config.roomid);
	}

	return checker;
}

export function CheckPresence({roomid, domain="" }, connection){
	
	const monitor = connection || io(domain,{
	    reconnection: true,
	    reconnectionDelay: 3000,
	    reconnectionAttempts: "Infinity",
        transports: ['websocket'],
        upgrade: false,
        rejectUnauthorized: true
	});

	/**/
    var currentState = 'free';
    /*overloading event*/
	var notify = (exist,rid)=> {/*`CheckPresence; Room ${rid} it's ${exist?'open':'free'}`*/};
 
    const state = new ReactiveVar(currentState);
    const makestate = exist => exist?'broadcasting':'free';

    const reCheck = () => {
    	monitor.emit('check-presence',roomid,(isBroadcastExists,id,data)=>{
       		if(currentState!=makestate(isBroadcastExists)) {
    			currentState=makestate(isBroadcastExists)
    			notify && typeof notify=='function' && notify(id,currentState);
    			state.set(makestate(isBroadcastExists));
    		}
            setTimeout(()=> reCheck(), 1000*60 );
    	});
    }
	
    monitor.on('room-status-changed', (id,status) => {
        console.log('room-status-changed',id,status);
    	notify && typeof notify=='function' && notify(id,makestate(status));
    	roomid==id && state.set(makestate(status));
    });

    monitor.on('connect', () => reCheck());
    monitor.on('error',(error)=> console.error("An error ocurred on CheckPresence ",error))
    monitor.on("reconnecting", (delay, attempt) => console.info(`Failed to reconnect. Lets try that again in ${delay}ms.`));


    return {
    	setRoomId:(id)=> roomid=id,
    	onNotify: (fn)=> notify=fn,
    	state:state,
    	socket:monitor,
        reCheck
    }
}

