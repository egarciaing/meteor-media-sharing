/*
 * 
 */
export class Common {
	constructor(){
		this.roles = {
			EMITTER:'emitter',
			CATCHER:'catcher',
			AWAIT:'awaiting'
		};

		this.role = this.roles.AWAIT;
		this.tmp = { };
	}

	setUserId(userid,cb){
		this.connection.changeUserId(userid,(cb&&typeof cb == "function")&& cb);
	}

	getUserId(){
		return this.connection.userid;
	}

	bothUserRole(isRoomExist){
		return isRoomExist ? this.setAsCatcher() : this.setAsEmitter();
	}

	setAsEmitter(){
		this.role = this.roles.EMITTER;
	}

	setAsCatcher(){
		this.role = this.roles.CATCHER;
	}

	get isOwner(){
		return this.connection.isInitiator;
	}

	set MessageEvent(message){
		if(message) this.connection.socketMessageEvent = message;
	}

	close(){
		this.connection.disconnect();
		this.stop();
        this.closeSocket();
	}

	stop(){
        this.connection.attachStreams.forEach(stream => stream.stop());
        this.connection.dontCaptureUserMedia = false;
	}

	disconnectAll(){
		this.connection.getAllParticipants().forEach((p)=> {
           this.connection.disconnectWith(p);
        });
	}

	setURL(url){
		this.connection.socketURL = url;
	}
}