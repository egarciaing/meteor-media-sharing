/*
 * @description Parse IceServers param string to standar configuration. 
 * @param str {String} 'stun:stun.l.google.com:19302, team%40rocket.chat:demo@turn:numb.viagenie.ca:3478, stun:23.21.150.121'
 * @return [{
 *   urls:String,
 *   credential?:String,
 *   username?: String
 *	}];
 */
export function ParseIceServerString(str){
	return str.split(',').map(chunk => {
		chunk = chunk.trim();
		if(!chunk.trim().match("@")) return { urls: chunk };
		let [credentials,urls] = chunk.split('@');
		let [credential,username] = credentials.split(':')
		return {
			urls,
			credential,
			username
		}
	}).sort( (p,n) => p&&p.urls&&!!p.urls.match(/^(stun+)/)? -1 : 1)
}

export function exitFullscreen(target){
    fn = document.webkitCancelFullScreen || document.mozCancelFullScreen || document.msExitFullscreen;
    return fn.call(document);
}

export function requestFullscreen(target){
    fn = target.webkitRequestFullscreen || target.mozRequestFullScreen || target.msRequestFullscreen;
    return fn.call(target);
}