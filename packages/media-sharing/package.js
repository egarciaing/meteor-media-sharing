Package.describe({
	name:"media-sharing",
	summary:"Tool based on RTCMultiConnection",
	git: "https://github.com/muaz-khan/RTCMultiConnection",
	version:'0.1.0'
});

Npm.depends({
	/*ws-wss depends*/
    "uws":"100.0.1",
    "fbr":"2.0.8",
    "bufferutil": "1.2.1",
    "utf-8-validate": "1.2.1",
	"socket.io": "2.2.0",
	"socket.io-client": "2.2.0",
	"meteor-node-stubs":"0.2.11",
	
	/*rtc depends*/
	"recordrtc": "5.5.4",
	'detectrtc':"1.3.9",
    "getstats":"1.0.2",
    "webrtc-adapter": "1.4.0",
    "webrtc-screen-capturing":"1.0.1",
    "multistreamsmixer":"1.0.8",
    "rtcmulticonnection-server":"1.3.1"
});


Package.onUse(function(api) {
	api.use([
		'ecmascript',
		'templating',
		'modules',
		'tracker',
		'reactive-var',
		'reactive-dict',
		'webapp',
	]);

	api.addFiles([
		'client/packages/socket.io.js',
		'client/packages/detectrtc.js',
		'client/packages/adapter.js',
		'client/main.js'
	], 'client');

	api.addFiles([
		'lib/utils.js',
		'lib/common.js',
		'lib/broadcast.js',
		'lib/defaults.js',
		'lib/checkPresence.js',
		'lib/createMediaElement.js',
		'lib/checkChromeExtension.js',
		'lib/screen-shared.js',
		'lib/index.js'
	], 'client');

	api.addFiles([
		'client/screen-shared/screenSharingWidget.html',
		'client/screen-shared/screenSharingWidget.css',
		'client/screen-shared/screenSharingWidget.js'
	], 'client');

	api.addFiles([
		'server/main.js'
	], 'server');

	api.export([
		'ScreenSharingHandler'
	],'client');
});