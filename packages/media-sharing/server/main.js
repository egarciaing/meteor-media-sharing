import io from 'socket.io';
import RTCMultiConnectionServer  from 'rtcmulticonnection-server';

export const StackRooms = new Map();

Meteor.startup(() => {
 	const ioServer = io(WebApp.httpServer,{ cookie: false });
 	
 	ioServer.on('connection', function(socket) {
	    const params = socket.handshake.query;

	    RTCMultiConnectionServer.addSocket(socket);

	    socket.on('room-status-changed',(roomid,data)=>{
	    	StackRooms.set(roomid,data);
	        socket.broadcast.emit('room-status-changed',roomid,data);
	    });

	    /* proximamente: notificar desconeccion inesperada de uuna transmision de sala.*/
	    /*socket.on('disconnect',()=>{
	    	socket.broadcast.emit('room-status-changed','roomid',{
	    		alive:false,
	    		status:'die'
	    	});
	    });*/
	    
	});
});