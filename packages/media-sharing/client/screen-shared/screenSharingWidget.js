import {Template} from 'meteor/templating';
import './screenSharingWidget.html';

import { Chrome } from '../../lib';
import { ScalableScreenSharing } from '../../lib/screen-shared';
import { BroadcastConnectedEvents } from '../../lib/defaults';
import { CreateMediaElement } from '../../lib/createMediaElement';
import { requestFullscreen, exitFullscreen } from '../../lib/utils';

import API from '../main';

Template.screenSharingWidget.onCreated(function(){

    const data = this.data;

    API.set(data.roomid,this);
    
    this.isFullscreen = new ReactiveVar(false);
    this.isPlayed = new ReactiveVar(false);

    this.sharing = new ScalableScreenSharing(data.roomid, { 
        useDefaultDevices: true,
        autoCreateMediaElement: false
    });

    this.sharing.Connected(BroadcastConnectedEvents.bind(this.sharing));
});

Template.screenSharingWidget.onDestroyed(function(){
    API.destroy(this.data.roomid);
    this.sharing.close();
    delete this.sharing;
});

Template.screenSharingWidget.onRendered(function(){
        
    /* Video element */
    this.sharing.connection.videosContainer = this.$('.video-container')[0]; 
    this.sharing.Preview = CreateMediaElement(null,{ local:false, parent:this.sharing.connection.videosContainer });// create media element
    

    /* Global api registration*/
    API.show(this.data.roomid).set(true);
    API.set(this.data.roomid,this);
    
    /* handling streaming event*/
    this.sharing.Streaming((event)=>{

        if(event.stream.isAudio) {
            delete event.mediaElement;
            return;
        }

        if(this.sharing.connection.isInitiator && event.type !== 'local') {
            return;
        }

        this.sharing.connection.isUpperUserLeft = false;

        this.sharing.Preview.setSourceStream(event);// set source stream

        if(event.mediaElement) {
            event.mediaElement.pause();
            delete event.mediaElement;
        }

        if (this.sharing.connection.isInitiator == false && event.type === 'remote') {
            // he is merely relaying the media
            this.sharing.connection.dontCaptureUserMedia = true;
            this.sharing.connection.attachStreams.push(event.stream);

            this.sharing.connection.sdpConstraints.mandatory = {
                OfferToReceiveAudio: false,
                OfferToReceiveVideo: false
            };

            this.sharing.getSocket(socket => {
                socket.emit('can-relay-broadcast');

                if(this.sharing.connection.DetectRTC.browser.name === 'Chrome') {
                    this.sharing.connection.getAllParticipants().forEach(remoteUserId => {
                        if(remoteUserId + '' != event.userid + '') {
                            let peer = this.sharing.connection.peers[remoteUserId].peer;
                            peer.getLocalStreams().forEach(localStream => peer.removeStream(localStream));
                            peer.addStream(event.stream);
                            this.sharing.connection.dontAttachStream = true;
                            this.sharing.connection.renegotiate(remoteUserId);
                            this.sharing.connection.dontAttachStream = false;
                        }
                    });
                }

                if(this.sharing.connection.DetectRTC.browser.name === 'Firefox') {
                    this.sharing.connection.getAllParticipants().forEach(remoteUserId => {
                        if(remoteUserId + '' != event.userid + '') {
                            this.sharing.connection.replaceTrack(event.stream, remoteUserId);
                        }
                    });
                }

                if(this.sharing.connection.DetectRTC.browser.name === 'Chrome') {
                    this.sharing.RecordStream(event.stream);
                }
            });
        }
    });

    /* hack for auto-open/join */
    if(this.data.autoOpenOrJoin){
        this.sharing.OpenOrJoin();
    }
});

Template.screenSharingWidget.helpers({
    owner(){
        return Template.instance().sharing.owner.get();
    },
    muted(){
        return Template.instance().sharing.muted.get();
    },
    played(){
        return Template.instance().isPlayed.get();
    },
    fullscreen(){
        return Template.instance().isFullscreen.get();
    },
    scope(){
        const ins =  Template.instance();
        const played = ins.isPlayed.get(),
              fullscreen = ins.isFullscreen.get(),
              muted = ins.sharing.muted.get();

        return {
            played,
            muted,
            fullscreen,
            ...ins.data
        }
    },
    isChrome(){
        return DetectRTC.browser.name == "Chrome";
    },
    chromeExtensionStatus(){
        return Chrome.MediaStatus.get();
    }
});

Template.screenSharingWidget.events({
    'click [data-action="open/join"]'(evt,ins){
        ins.sharing.OpenOrJoin();
    },
    'click [data-action="disconnect"]'(evt,ins){
        ins.sharing.disconnect();
    },

    'click [data-action="mute/unmute"]'(evt,ins){
        ins.sharing.mute();
    },
    'click [data-action="play/pause"]'(evt,ins){
        ins.sharing.Preview.paused?ins.sharing.Preview.play():ins.sharing.Preview.pause();
        ins.isPlayed.set(!ins.sharing.Preview.paused);
        ins.sharing.disableVideo();
        ins.sharing.mute();
    },
    'click [data-action="snapshot"]'(evt,ins){
        if(ins.sharing.Preview.paused) return;
        ins.sharing.snapshot().then((data)=>{
            const psa = ins.$("#downloader")[0];
            
            psa.href = URL.createObjectURL(data.blob);
            psa.click();

            setTimeout(()=> {
                URL.revokeObjectURL(psa.href);
                psa.href = null;
            },5000);
        });
    },


    'click [data-action="fullscreen"]'(evt,ins){
        evt.preventDefault();

        const fullscreen = ins.isFullscreen.get();
        ins.isFullscreen.set(!fullscreen);

        fullscreen ?  exitFullscreen(ins.firstNode) : requestFullscreen(document.querySelector(".screen-sharing-widget"));
    },
    'click [data-action="request-fullscreen"]'(evt,ins){
        evt.preventDefault();

        requestFullscreen(ins.firstNode);
    },
    'click [data-action="exit-fullscreen"]'(evt,ins){
        evt.preventDefault();

        exitFullscreen(ins.firstNode);
    },
    
    'click [data-action="attach/dettach"]'(evt,ins){
        ins.sharing.Preview.attached() ? ins.sharing.Preview.dettach() : ins.sharing.Preview.attach(ins.sharing.connection.videosContainer);
    },
    'click [data-action="attach"]'(evt,ins){
        !ins.sharing.Preview.attached() && ins.sharing.Preview.attach(ins.sharing.connection.videosContainer);
    },
    'click [data-action="dettach"]'(evt,ins){
        ins.sharing.Preview.attached() && ins.sharing.Preview.dettach();
    },
    'click [data-action="exit"]'(evt,ins){
        API.Room(ins.data.roomid).set(false);
    }
});