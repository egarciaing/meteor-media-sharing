import { ParseIceServerString, Chrome } from '../lib';

Meteor.startup(function(){
	Template.registerHelper('hasChild', function () { 
		return (Template.instance().view.templateContentBlock) ? true : false;
	});

	Chrome.getExtensionStatus((status)=>{
		console.log('Extension status ',status);
	});
})

/*
 * API 
 * @description Permite gestionar multiples instancias de ScreenSharing en un mismo momento.
 */
var IceServers = null;
const ShowByRoomsMap = new Map();
const MediaSharingMap = new Map();
const MediaSharingDep = new Tracker.Dependency;

const show = (id,initialstate=false)=>{
	if(!ShowByRoomsMap.has(id)){
		ShowByRoomsMap.set(id, new ReactiveVar(initialstate))
	}
	return ShowByRoomsMap.get(id);
}

ScreenSharingHandler = {
	show,
	Room:show,/* compativilidad con la vieja api*/
	has:(id) => {
		MediaSharingDep.depend();
		return MediaSharingMap.has(id);
	},
	get(id){
		MediaSharingDep.depend();
		return MediaSharingMap.get(id);
	},
	set(id,instanceRef){
		MediaSharingMap.set(id,instanceRef);
		MediaSharingDep.changed();
	},
	destroy(id){
		MediaSharingMap.delete(id);
		MediaSharingDep.changed();
	},
	setIceServers(servers){
		servers && typeof servers=='string' && (servers=ParseIceServerString(servers));
		servers && (IceServers = servers);
	},
	getIceServers:()=> IceServers,
}

export default ScreenSharingHandler;